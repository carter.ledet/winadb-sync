@echo off

rem # run shell script
:script
del sync-errors.txt > nul 2>&1
bash winadb-sync.bash %* || goto err

goto end

:err
echo.
echo usage: winadb [pull^|push] SOURCEDIR TARGETDIR

:end
if exist sync-errors.txt for /f "tokens=* delims=" %%t in (sync-errors.txt) do del "%%t" > nul 2>&1
del source.list > nul 2>&1
echo.
pause
exit /b 1
